package com.sample.project.SampleProject.methods;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface TestBinding {
	
	//@Output("testchannel")
   // MessageChannel test();
	
	  String TESTCHANNEL = "testchannel";

	    @Input(TESTCHANNEL)
	    SubscribableChannel test();

}
