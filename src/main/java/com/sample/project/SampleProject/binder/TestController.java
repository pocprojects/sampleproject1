package com.sample.project.SampleProject.binder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.web.bind.annotation.GetMapping;

import com.sample.project.SampleProject.methods.TestBinding;

//@RestController
//@EnableBinding(TestBinding.class)
public class TestController {

	@Autowired
	TestBinding testBinding;

	@GetMapping("/test1")
	public String publish() {
		String greeting = "Hello, " + "pankaj" + "!";
		
		//testBinding.test().send(MessageBuilder.withPayload(greeting).build());
		return "messageSend";
	}

}
