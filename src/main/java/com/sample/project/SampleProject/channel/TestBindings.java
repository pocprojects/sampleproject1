package com.sample.project.SampleProject.channel;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface TestBindings {
	
    
    String WAVFORM = "waveform";

    @Input(WAVFORM)
    SubscribableChannel waveform();

}
