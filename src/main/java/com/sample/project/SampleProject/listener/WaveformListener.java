package com.sample.project.SampleProject.listener;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

import com.sample.project.SampleProject.methods.TestBinding;



@EnableBinding(TestBinding.class)
public class WaveformListener {
	

	
    @StreamListener(TestBinding.TESTCHANNEL)
    public void liveWaveformDataListener(String waveformData) {
    	
    	System.out.println("-------------------"+waveformData);
    	
    	
        
        System.out.println("Process completed");
        

    }
}